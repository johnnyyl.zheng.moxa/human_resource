# **Human Resource**

 - This is a simple server that stores data on human resources.
 - The human resources can be accessed by RESTful API.
 - The server listens on localhost:8888.

## **Getting started**

### **Ubuntu**

1. This guide convers Ubuntu 22.04.2 LTS

   ```
   $ sudo apt update
   $ sudo apt upgrade
   ```

2. Install dependencies (sqlite3)

   ```
   $ sudo apt-get install sqlite3 libsqlite3-dev
   ```

3. Get Human Resource

   ```
   $ git clone https://gitlab.com/moxa/ibg/software/practice/moxa-sw-practice.git
   $ cd moxa-sw-practice/
   $ git fetch
   $ git switch -c johnny/socket_programming origin/johnny/socket_programming
   $ cd human_resource/
   ```

4. Build and run the server

   ```
   $ make
   ```

## **More Information**

- [REST API](https://app.swaggerhub.com/apis-docs/JOHNNYYLZHENG/Human_Resource/1.0.0)

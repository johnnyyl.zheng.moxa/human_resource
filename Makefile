FILES := human_resource.c

ASTYLE := astyle
ASTYLE_OPTIONS := -A1 -c -f -n -p -s4 -w -H -U -xg -k3 -W3 -j -z2

CC := gcc
CFLAGS := -O0 -g -Wall -Wextra -Wno-deprecated-declarations -pedantic-errors
LIBS := -lsqlite3 -pthread
LIBS_LAPUTA := -lcfgapi -lparson

all: human_resource

.PHONY: human_resource ci
human_resource: human_resource.c parson.c
	$(ASTYLE) $(ASTYLE_OPTIONS) $(FILES)
	$(CC) $(CFLAGS) -o $@ human_resource.c parson.c $(LIBS)
	rm -f *.db
	./$@

ci: human_resource.c
	$(CC) $(CFLAGS) -o $@ human_resource.c $(LIBS) $(LIBS_LAPUTA)

clean:
	rm -f human_resource *.o


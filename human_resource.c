/*
 * \brief
 * Implementation a simple server that stores data on human resources that can be accessed by RESTful API.
 *
 * \copyright
 * Copyright (C) MOXA Inc. All rights reserved.
 * This software is distributed under the terms of the
 * MOXA License.  See the file COPYING-MOXA for details.
 *
 * \date    2023/05/16
 *          First release
 * \author  Johnny Zheng
 */

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <pthread.h>
#include <sys/select.h>
#include <unistd.h>
#include <netdb.h>
#include <signal.h>
#include <errno.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <sqlite3.h>
#include <semaphore.h>

#include "parson.h"

#define PORT ("8888")
#define BACKLOG (10)
#define DATABASE_FILE ("profiles.db")
#define BUFFER_SIZE (1024)
#define INITIAL_BUFFER_SIZE (100)
#define MAX_BUFFER_SIZE (1024)
#define NAME_SIZE (20)
#define GENDER_SIZE (10)
#define EMAIL_SIZE (100)
#define MAX_CLIENTS (10)

typedef struct _client_info client_info;
typedef struct _profile profile;

struct _client_info
{
    char ip_addr[INET6_ADDRSTRLEN];
    int fd;
};

struct _profile
{
    int id;
    int age;
    char name[NAME_SIZE];
    char gender[GENDER_SIZE];
    char email[EMAIL_SIZE];
};

typedef enum
{
    ERROR_NO_ERROR         = 0,
    ERROR_GET_ADDR_INFO    = 1,
    ERROR_SET_SOCK_OPT     = 2,
    ERROR_FAILED_TO_BIND   = 3,
    ERROR_FAILED_TO_LISTEN = 4,
    ERROR_CREATE_THREAD    = 5,
    ERROR_ACCEPT           = 6,
    ERROR_OPEN_DB          = 7,
    ERROR_ACCESS_DB        = 8,
    ERROR_CREATE_TABLE     = 9,
    ERROR_EXIT             = 10,
    ERROR_INIT             = 11,
} error_type;

typedef enum
{
    RECV_STOP     = 0,
    RECV_CONTINUE = 1,
} recv_status;

typedef enum
{
    HTTP_COMPLETE   = 0,
    HTTP_INCOMPLETE = 1,
} http_status;

static pthread_mutex_t g_db_mutex;
static sem_t g_thread_sem;

error_type init_mutex_and_semaphore(void);
void cleanup(void *arg);
error_type prepare_database(void);
error_type open_database(sqlite3 **db);
void close_database(sqlite3 *db);
error_type start_server(void);
error_type setup_socket_and_listen(int *socket_fd);
error_type setup_socket(int *socket_fd);
error_type get_server_info(struct addrinfo **server_info);
int create_socket(struct addrinfo *server_info);
error_type set_socket_option(int socket_fd);
error_type bind_socket(int socket_fd, struct addrinfo *server_info);
error_type accept_connection(int socket_fd);
error_type accept_new_connection_or_exit(int socket_fd);
error_type process_connection(int socket_fd, const char *ip_addr);
void *get_in_addr(struct sockaddr *sa);
void *connection_handler(void *);
char *get_request_and_process(char *buffer, size_t buffer_size, client_info info);
recv_status recv_message_to_buffer(char *buffer, size_t buffer_size, size_t *received_size, client_info info);
http_status check_http_message_completeness(const char *message);
char *reallocate_recv_buffer(char *buffer, size_t *buffer_size);
void handle_request_message(const char *message, int socket_fd);
void handle_home_page_get(int socket_fd);
void handle_error_message(const char *error_message, int socket_fd);
void handle_profile_page_post(const char *message, int socket_fd);
void handle_profile_page_get(int socket_fd);
void handle_profile_page_patch(const char *message, int socket_fd);
void handle_profile_page_delete(const char *message, int socket_fd);
size_t get_body_size(const char *message);
profile extract_profile_from_http(const char *message);
const char *find_http_body(const char *message);
void extract_profile_from_body(profile *extracted_profile, const char *body);
int extract_id_from_query(const char *message);
unsigned get_parameter_value(const char *query_string, const char *parameter_name);
unsigned convert_string_to_uint(const char *target);
void copy_existing_string(char *dst, const char *src, const size_t max_buffer_size);
char *get_all_profiles(void);
JSON_Value *get_profile_from_db(sqlite3_stmt *stmt);
void send_all_the_profiles(int client_socket);
void send_all(int socket_fd, const char *message, size_t message_size);
error_type insert_profile_to_database(profile *new_profile);
error_type update_profile_in_database(profile *new_profile);
error_type delete_profile_in_database(int profile_id);

int main(void)
{
    int status = ERROR_NO_ERROR;

    status = init_mutex_and_semaphore();

    if (status != ERROR_NO_ERROR)
    {
        return 0;
    }

    pthread_cleanup_push(cleanup, NULL);

    printf("setup database...\n");
    status = prepare_database();

    if (status != ERROR_NO_ERROR)
    {
        printf("Something is wrong with database, error code %d\n", status);
        pthread_exit(NULL);
    }

    printf("setup server...\n");
    status = start_server();

    if (status != ERROR_NO_ERROR && status != ERROR_EXIT)
    {
        printf("Something is wrong with server, error code %d\n", status);
        pthread_exit(NULL);
    }

    pthread_exit(NULL);
    pthread_cleanup_pop(1);

    return 0;
}

error_type init_mutex_and_semaphore(void)
{
    if (pthread_mutex_init(&g_db_mutex, NULL) != 0)
    {
        perror("main: mutex init");
        return ERROR_INIT;
    }

    if (sem_init(&g_thread_sem, 0, MAX_CLIENTS) != 0)
    {
        perror("main: semaphore init");
        pthread_mutex_destroy(&g_db_mutex);
        return ERROR_INIT;
    }

    return ERROR_NO_ERROR;
}

void cleanup(void *arg)
{
    (void)arg;
    int sem_value = 0;

    sem_getvalue(&g_thread_sem, &sem_value);

    while (sem_value < MAX_CLIENTS)
    {
        sleep(1);
        sem_getvalue(&g_thread_sem, &sem_value);
    }

    pthread_mutex_destroy(&g_db_mutex);
    sem_destroy(&g_thread_sem);
}

error_type prepare_database(void)
{
    sqlite3 *db = NULL;
    error_type status = ERROR_NO_ERROR;

    status = open_database(&db);

    if (status != ERROR_NO_ERROR)
    {
        return status;
    }

    const char *create_table_query = "CREATE TABLE IF NOT EXISTS profiles ("
                                     "id INTEGER PRIMARY KEY AUTOINCREMENT,"
                                     "name TEXT NOT NULL,"
                                     "age INTEGER NOT NULL,"
                                     "gender TEXT NOT NULL,"
                                     "email TEXT NOT NULL)";


    if (sqlite3_exec(db, create_table_query, 0, 0, 0) != SQLITE_OK)
    {
        fprintf(stderr, "Failed to create table: %s\n", sqlite3_errmsg(db));
        status = ERROR_CREATE_TABLE;
    }

    close_database(db);

    return status;
}

error_type open_database(sqlite3 **db)
{
    if (db == NULL)
    {
        return ERROR_OPEN_DB;
    }

    pthread_mutex_lock(&g_db_mutex);

    if (sqlite3_open(DATABASE_FILE, db) != SQLITE_OK)
    {
        fprintf(stderr, "Failed to open database: %s\n", sqlite3_errmsg(*db));
        pthread_mutex_unlock(&g_db_mutex);
        return ERROR_OPEN_DB;
    }

    return ERROR_NO_ERROR;
}

void close_database(sqlite3 *db)
{
    if (db == NULL)
    {
        return;
    }

    while (sqlite3_close(db) == SQLITE_BUSY)
    {
        sqlite3_stmt *stmt = sqlite3_next_stmt(db, NULL);

        if (stmt == NULL)
        {
            fprintf(stderr, "Failed to close database: %s\n", sqlite3_errmsg(db));
            break;
        }

        sqlite3_finalize(stmt);
    }

    pthread_mutex_unlock(&g_db_mutex);
}

error_type start_server()
{
    int status = ERROR_NO_ERROR;
    int socket_fd;

    status = setup_socket_and_listen(&socket_fd);

    if (status != ERROR_NO_ERROR)
    {
        printf("server: waiting for connections...\n");
    }

    while (status == ERROR_NO_ERROR)
    {
        status = accept_new_connection_or_exit(socket_fd);
    }

    close(socket_fd);
    return status;
}

error_type setup_socket_and_listen(int *socket_fd)
{
    int status = setup_socket(socket_fd);

    if (status != ERROR_NO_ERROR)
    {
        return status;
    }

    if (listen(*socket_fd, BACKLOG) == -1)
    {
        perror("server: listen");
        return ERROR_FAILED_TO_LISTEN;
    }

    return ERROR_NO_ERROR;
}

error_type setup_socket(int *socket_fd)
{
    int status = ERROR_NO_ERROR;
    struct addrinfo *server_info = NULL, *p = NULL;

    status = get_server_info(&server_info);

    if (status != ERROR_NO_ERROR)
    {
        return status;
    }

    for (p = server_info; p != NULL; p = p->ai_next)
    {
        *socket_fd = create_socket(p);

        if (*socket_fd == -1)
        {
            continue;
        }

        status = set_socket_option(*socket_fd);

        if (status != ERROR_NO_ERROR)
        {
            freeaddrinfo(server_info);
            return status;
        }

        if (bind_socket(*socket_fd, p) == ERROR_NO_ERROR)
        {
            break;
        }
    }

    freeaddrinfo(server_info);

    if (p == NULL)
    {
        fprintf(stderr, "server: failed to bind\n");
        status = ERROR_FAILED_TO_BIND;
    }

    return status;
}

error_type get_server_info(struct addrinfo **server_info)
{
    int status;
    struct addrinfo hints;

    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    if ((status = getaddrinfo(NULL, PORT, &hints, server_info)) != 0)
    {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(status));
        return ERROR_GET_ADDR_INFO;
    }

    return ERROR_NO_ERROR;
}

int create_socket(struct addrinfo *server_info)
{
    int socket_fd = 0;
    socket_fd = socket(server_info->ai_family,
                       server_info->ai_socktype, server_info->ai_protocol);

    if (socket_fd == -1)
    {
        perror("server: socket");
    }

    return socket_fd;
}

error_type set_socket_option(int socket_fd)
{
    const int option_value = 1;

    if (setsockopt(socket_fd, SOL_SOCKET, SO_REUSEADDR, &option_value, sizeof(int)) == -1)
    {
        perror("server: setsockopt");
        return ERROR_SET_SOCK_OPT;
    }

    return ERROR_NO_ERROR;
}

error_type bind_socket(int socket_fd, struct addrinfo *server_info)
{
    if (bind(socket_fd, server_info->ai_addr, server_info->ai_addrlen) == -1)
    {
        close(socket_fd);
        perror("server: bind");
        return ERROR_FAILED_TO_BIND;
    }

    return ERROR_NO_ERROR;
}

error_type accept_connection(int socket_fd)
{
    int status = ERROR_NO_ERROR;
    struct sockaddr_storage client_addresses;
    socklen_t sin_size = sizeof(client_addresses);
    int client_fd;
    char client_addr[INET6_ADDRSTRLEN];

    client_fd = accept(socket_fd, (struct sockaddr *)&client_addresses, &sin_size);

    if (client_fd == -1)
    {
        perror("server: accept");
        status = ERROR_ACCEPT;
        return status;
    }

    inet_ntop(client_addresses.ss_family,
              get_in_addr((struct sockaddr *)&client_addresses),
              client_addr, (INET6_ADDRSTRLEN));

    status = process_connection(client_fd, client_addr);

    return status;
}

error_type accept_new_connection_or_exit(int socket_fd)
{
    error_type status = ERROR_NO_ERROR;
    fd_set read_fds;

    FD_ZERO(&read_fds);
    FD_SET(STDIN_FILENO, &read_fds);
    FD_SET(socket_fd, &read_fds);

    if (select(socket_fd + 1, &read_fds, NULL, NULL, NULL) == -1)
    {
        perror("server: select");
        return ERROR_ACCEPT;
    }

    if (FD_ISSET(STDIN_FILENO, &read_fds))
    {
        int ch = getchar();

        if (ch == 'q' || ch == 'Q')
        {
            return ERROR_EXIT;
        }
    }

    if (FD_ISSET(socket_fd, &read_fds))
    {
        status = accept_connection(socket_fd);
    }

    return status;
}

error_type process_connection(int client_fd, const char *client_addr)
{
    error_type status = ERROR_NO_ERROR;
    pthread_t thread_id;
    client_info *client = (client_info *)malloc(sizeof(client_info));

    if (client == NULL)
    {
        perror("server: malloc client info");
        status = ERROR_ACCEPT;
        close(client_fd);
        return status;
    }

    client->fd = client_fd;
    snprintf(client->ip_addr, INET6_ADDRSTRLEN, "%s", client_addr);

    sem_wait(&g_thread_sem);

    if (pthread_create(&thread_id, NULL, connection_handler, (void *)client) < 0)
    {
        perror("server: pthread_create");
        free(client);
        close(client_fd);
        sem_post(&g_thread_sem);
        status = ERROR_CREATE_THREAD;
        return status;
    }

    pthread_detach(thread_id);

    printf("server: got connection from %s\n", client_addr);

    return status;
}

void *get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET)
    {
        return &(((struct sockaddr_in *)sa)->sin_addr);
    }

    return &(((struct sockaddr_in6 *)sa)->sin6_addr);
}

void *connection_handler(void *arg)
{
    client_info my_info = *(client_info *)arg;
    char *buffer = malloc(sizeof(char) * INITIAL_BUFFER_SIZE);
    size_t buffer_size = INITIAL_BUFFER_SIZE;

    if (buffer == NULL)
    {
        perror("server: malloc recv buffer");
    }
    else
    {
        buffer = get_request_and_process(buffer, buffer_size, my_info);
        free(buffer);
    }

    close(my_info.fd);
    free(arg);
    sem_post(&g_thread_sem);

    return 0;
}

char *get_request_and_process(char *buffer, size_t buffer_size, client_info info)
{
    size_t received_size = 0;

    while (1)
    {
        if (recv_message_to_buffer(buffer,
                                   buffer_size - 1, /* reserve last byte as '\0' */
                                   &received_size, info) == RECV_STOP)
        {
            break;
        }

        if (check_http_message_completeness(buffer) == HTTP_COMPLETE)
        {
            handle_request_message(buffer, info.fd);
            break;
        }

        char *new_buffer = reallocate_recv_buffer(buffer, &buffer_size);

        if (new_buffer == NULL)
        {
            perror("server: reallocate recv buffer");
            break;
        }

        buffer = new_buffer;
    }

    return buffer;
}

recv_status recv_message_to_buffer(char *buffer, size_t buffer_size, size_t *received_size, client_info info)
{
    ssize_t bytes_received = recv(info.fd, buffer + (*received_size), buffer_size - (*received_size), 0);

    if (bytes_received > 0)
    {
        *received_size += bytes_received;
        buffer[*received_size] = '\0';
        return RECV_CONTINUE;
    }
    else if (bytes_received == 0)
    {
        printf("server: client (%s) disconnected\n", info.ip_addr);
    }
    else
    {
        perror("server: recv error");
    }

    return RECV_STOP;
}

http_status check_http_message_completeness(const char *message)
{
    const char *end_of_headers = strstr(message, "\r\n\r\n");
    size_t header_size = 0;
    size_t body_size = 0;

    if (end_of_headers == NULL)
    {
        return HTTP_INCOMPLETE;
    }

    if ((strstr(message, "POST /profile") == NULL)
            && (strstr(message, "PATCH /profile") == NULL))
    {
        return HTTP_COMPLETE;
    }

    header_size = (end_of_headers + 4) - message;
    body_size = get_body_size(message);

    if (strlen(message) - header_size < body_size)
    {
        return HTTP_INCOMPLETE;
    }

    return HTTP_COMPLETE;
}

char *reallocate_recv_buffer(char *buffer, size_t *buffer_size)
{
    if (*buffer_size == MAX_BUFFER_SIZE)
    {
        fprintf(stderr, "Request message exceeds maximum buffer size\n");
        return NULL;
    }

    (*buffer_size) *= 2;

    if (*buffer_size > MAX_BUFFER_SIZE)
    {
        *buffer_size = MAX_BUFFER_SIZE;
    }

    return realloc(buffer, *buffer_size);
}

void handle_request_message(const char *message, int socket_fd)
{
    if (message == NULL)
    {
        handle_error_message("Got an empty message", socket_fd);
    }
    else if (strstr(message, "GET / ") != NULL)
    {
        handle_home_page_get(socket_fd);
    }
    else if (strstr(message, "GET /profile") != NULL)
    {
        handle_profile_page_get(socket_fd);
    }
    else if (strstr(message, "POST /profile") != NULL)
    {
        handle_profile_page_post(message, socket_fd);
    }
    else if (strstr(message, "PATCH /profile") != NULL)
    {
        handle_profile_page_patch(message, socket_fd);
    }
    else if (strstr(message, "DELETE /profile") != NULL)
    {
        handle_profile_page_delete(message, socket_fd);
    }
    else
    {
        handle_error_message("Page not found", socket_fd);
    }
}

void handle_home_page_get(int socket_fd)
{
    const char *message = "HTTP/1.1 200 OK\r\nContent-Length: 14\r\n\r\nHello World!\r\n";
    send_all(socket_fd, message, strlen(message));
}

void handle_error_message(const char *error_message, int socket_fd)
{
    char *message = malloc(sizeof(char) * BUFFER_SIZE);
    snprintf(message, BUFFER_SIZE,
             "HTTP/1.1 400 ERROR\r\nContent-Length: %zu\r\n\r\n{\"error\": \"%s\"}\r\n",
             strlen(error_message) + 15, error_message);
    send_all(socket_fd, message, strlen(message));
    free(message);
}

void send_all(int socket_fd, const char *message, size_t message_size)
{
    size_t offset = 0;
    ssize_t sent_size = 0;

    while (message_size > 0)
    {
        sent_size = send(socket_fd, &message[offset], message_size, 0);

        if (sent_size == -1)
        {
            perror("server: send_all");
            break;
        }

        message_size -= sent_size;
        offset += sent_size;
    }
}

void handle_profile_page_post(const char *message, int socket_fd)
{
    profile new_profile;

    new_profile = extract_profile_from_http(message);

    if (insert_profile_to_database(&new_profile) == ERROR_OPEN_DB)
    {
        handle_error_message("Failed to create profile", socket_fd);
    }
    else
    {
        send_all_the_profiles(socket_fd);
    }
}

void handle_profile_page_get(int socket_fd)
{
    send_all_the_profiles(socket_fd);
}

void send_all_the_profiles(int client_socket)
{
    char *message = malloc(sizeof(char) * BUFFER_SIZE);
    char *serialized_string = NULL;

    serialized_string = get_all_profiles();

    if (serialized_string == NULL)
    {
        handle_error_message("Failed to get profiles", client_socket);
        free(message);
        return;
    }

    snprintf(message, BUFFER_SIZE, "HTTP/1.1 200 OK\r\nContent-Length: %zu\r\n\r\n%s\r\n",
             strlen(serialized_string) + 2, serialized_string);
    send_all(client_socket, message, strlen(message));

    json_free_serialized_string(serialized_string);
    free(message);
}

char *get_all_profiles(void)
{
    const char *sql = "SELECT * FROM profiles";
    sqlite3_stmt *stmt;
    char *serialized_string = NULL;
    JSON_Value *members_value = json_value_init_array();
    JSON_Value *root_value = json_value_init_object();
    JSON_Object *root_object = json_value_get_object(root_value);
    sqlite3 *db = NULL;

    if (open_database(&db) != ERROR_NO_ERROR)
    {
        json_value_free(members_value);
        json_value_free(root_value);
        return serialized_string;
    }

    if (sqlite3_prepare_v2(db, sql, -1, &stmt, NULL) != SQLITE_OK)
    {
        fprintf(stderr, "Failed to get profiles: %s\n", sqlite3_errmsg(db));
        json_value_free(members_value);
        json_value_free(root_value);
        return serialized_string;
    }

    while (sqlite3_step(stmt) == SQLITE_ROW)
    {
        JSON_Value *profile_value = get_profile_from_db(stmt);
        json_array_append_value(json_value_get_array(members_value), profile_value);
    }

    json_object_set_value(root_object, "members", members_value);

    sqlite3_finalize(stmt);
    close_database(db);
    serialized_string = json_serialize_to_string_pretty(root_value);

    json_value_free(root_value);
    return serialized_string;
}

JSON_Value *get_profile_from_db(sqlite3_stmt *stmt)
{
    JSON_Value *profile_value = json_value_init_object();
    JSON_Object *profile_object = json_value_get_object(profile_value);

    if (stmt == NULL)
    {
        return profile_value;
    }

    json_object_set_number(profile_object, "id", (double)sqlite3_column_int(stmt, 0));
    json_object_set_string(profile_object, "name", (char *)sqlite3_column_text(stmt, 1));
    json_object_set_string(profile_object, "gender", (char *)sqlite3_column_text(stmt, 3));
    json_object_set_number(profile_object, "age", (double)sqlite3_column_int(stmt, 2));
    json_object_set_string(profile_object, "email", (char *)sqlite3_column_text(stmt, 4));

    return profile_value;
}

void handle_profile_page_patch(const char *message, int socket_fd)
{
    profile new_profile;

    new_profile = extract_profile_from_http(message);

    if (update_profile_in_database(&new_profile) == ERROR_OPEN_DB)
    {
        handle_error_message("Failed to update profile", socket_fd);
    }
    else
    {
        send_all_the_profiles(socket_fd);
    }
}

void handle_profile_page_delete(const char *message, int socket_fd)
{
    int profile_id = 0;

    profile_id = extract_id_from_query(message);

    if (delete_profile_in_database(profile_id) == ERROR_OPEN_DB)
    {
        handle_error_message("Failed to delete profile", socket_fd);
    }
    else
    {
        send_all_the_profiles(socket_fd);
    }

}

size_t get_body_size(const char *message)
{
    return (size_t)get_parameter_value(message, "Content-Length:");
}

const char *find_http_body(const char *message)
{
    const char *body = NULL;

    if (message == NULL)
    {
        return NULL;
    }

    body = strstr(message, "\r\n\r\n");

    return (body == NULL) ? NULL : (body + 4);
}

int extract_id_from_query(const char *message)
{
    const char *query_start = strchr(message, '?');

    if (query_start == NULL)
    {
        return -1;
    }

    /* Move past the '?' character */
    return get_parameter_value(query_start + 1, "id");
}

unsigned get_parameter_value(const char *query_string, const char *parameter_name)
{
    const char *value_start = strstr(query_string, parameter_name);

    if (value_start == NULL)
    {
        return -1;
    }

    /* Move past the parameter name and '=' character */
    value_start += strlen(parameter_name) + 1;
    return convert_string_to_uint(value_start);
}

unsigned convert_string_to_uint(const char *target)
{
    unsigned result = 0, offset = 0;
    const unsigned max_offset = 9; /* make result = [0, 999999999] */
    size_t target_len = 0;

    if (target == NULL)
    {
        return 0;
    }

    target_len = strlen(target);

    if (target_len > max_offset)
    {
        target_len = max_offset;
    }

    while (offset < target_len)
    {
        if (target[offset] < '0' || target[offset] > '9')
        {
            break;
        }

        result = result * 10 + (target[offset] - '0');
        offset++;
    }

    return result;
}

profile extract_profile_from_http(const char *message)
{
    profile extracted_profile;
    const char *body = NULL;

    /* Initialize profile */
    extracted_profile.id = 0;
    extracted_profile.age = 0;
    snprintf(extracted_profile.name, NAME_SIZE, "unknown");
    snprintf(extracted_profile.gender, GENDER_SIZE, "unknown");
    snprintf(extracted_profile.email, EMAIL_SIZE, "unknown");

    if (message == NULL)
    {
        return extracted_profile;
    }

    body = find_http_body(message);
    extract_profile_from_body(&extracted_profile, body);

    return extracted_profile;
}

void extract_profile_from_body(profile *extracted_profile, const char *body)
{
    JSON_Value *profile_value = NULL;
    JSON_Object *profile_object = NULL;

    if (extracted_profile == NULL || body == NULL)
    {
        return;
    }

    profile_value = json_parse_string(body);

    if (json_value_get_type(profile_value) != JSONObject)
    {
        return;
    }

    profile_object = json_value_get_object(profile_value);

    extracted_profile->id = (int)json_object_get_number(profile_object, "id");
    extracted_profile->age = (int)json_object_get_number(profile_object, "age");
    copy_existing_string(extracted_profile->name,
                         json_object_get_string(profile_object, "name"), NAME_SIZE);
    copy_existing_string(extracted_profile->gender,
                         json_object_get_string(profile_object, "gender"), GENDER_SIZE);
    copy_existing_string(extracted_profile->email,
                         json_object_get_string(profile_object, "email"), EMAIL_SIZE);

    json_value_free(profile_value);
}

void copy_existing_string(char *dst, const char *src, const size_t max_buffer_size)
{
    if (dst == NULL || src == NULL)
    {
        return;
    }

    snprintf(dst, max_buffer_size, "%s", src);
}

error_type insert_profile_to_database(profile *new_profile)
{
    const char *insert_query = NULL;
    sqlite3_stmt *stmt = NULL;
    error_type status = ERROR_NO_ERROR;
    sqlite3 *db = NULL;

    status = open_database(&db);

    if (status != ERROR_NO_ERROR)
    {
        return status;
    }

    insert_query = "INSERT INTO profiles (name, age, gender, email) VALUES (?, ?, ?, ?)";

    if (sqlite3_prepare_v2(db, insert_query, -1, &stmt, NULL) != SQLITE_OK)
    {
        fprintf(stderr, "Failed to prepare insertion query: %s\n", sqlite3_errmsg(db));
        return ERROR_OPEN_DB;
    }

    sqlite3_bind_text(stmt, 1, new_profile->name, -1, SQLITE_STATIC);
    sqlite3_bind_int(stmt, 2, new_profile->age);
    sqlite3_bind_text(stmt, 3, new_profile->gender, -1, SQLITE_STATIC);
    sqlite3_bind_text(stmt, 4, new_profile->email, -1, SQLITE_STATIC);

    if (sqlite3_step(stmt) != SQLITE_DONE)
    {
        fprintf(stderr, "Failed to create profile: %s\n", sqlite3_errmsg(db));
        status = ERROR_OPEN_DB;
    }

    sqlite3_finalize(stmt);
    close_database(db);

    return status;
}

error_type update_profile_in_database(profile *new_profile)
{
    const char *update_query = NULL;
    sqlite3_stmt *stmt = NULL;
    error_type status = ERROR_NO_ERROR;
    sqlite3 *db = NULL;

    status = open_database(&db);

    if (status != ERROR_NO_ERROR)
    {
        return status;
    }

    update_query = "UPDATE profiles SET name = ?, age = ?, gender = ?, email = ? WHERE id = ?";

    if (sqlite3_prepare_v2(db, update_query, -1, &stmt, NULL) != SQLITE_OK)
    {
        fprintf(stderr, "Failed to prepare update query: %s\n", sqlite3_errmsg(db));
        return ERROR_OPEN_DB;
    }

    sqlite3_bind_text(stmt, 1, new_profile->name, -1, SQLITE_STATIC);
    sqlite3_bind_int(stmt, 2, new_profile->age);
    sqlite3_bind_text(stmt, 3, new_profile->gender, -1, SQLITE_STATIC);
    sqlite3_bind_text(stmt, 4, new_profile->email, -1, SQLITE_STATIC);
    sqlite3_bind_int(stmt, 5, new_profile->id);

    if (sqlite3_step(stmt) != SQLITE_DONE)
    {
        fprintf(stderr, "Failed to update profile: %s\n", sqlite3_errmsg(db));
        status = ERROR_OPEN_DB;
    }

    sqlite3_finalize(stmt);
    close_database(db);

    return status;
}

error_type delete_profile_in_database(int profile_id)
{
    const char *delete_query = NULL;
    sqlite3_stmt *stmt = NULL;
    error_type status = ERROR_NO_ERROR;
    sqlite3 *db = NULL;

    status = open_database(&db);

    if (status != ERROR_NO_ERROR)
    {
        return status;
    }

    delete_query = "DELETE FROM profiles WHERE id = ?";

    if (sqlite3_prepare_v2(db, delete_query, -1, &stmt, NULL) != SQLITE_OK)
    {
        fprintf(stderr, "Failed to prepare deletion query: %s\n", sqlite3_errmsg(db));
        return ERROR_OPEN_DB;
    }

    sqlite3_bind_int(stmt, 1, profile_id);

    if (sqlite3_step(stmt) != SQLITE_DONE)
    {
        fprintf(stderr, "Failed to delete profile: %s\n", sqlite3_errmsg(db));
        status = ERROR_OPEN_DB;
    }

    sqlite3_finalize(stmt);
    close_database(db);

    return status;
}
